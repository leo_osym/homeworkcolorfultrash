package com.primeiro.homeworkcolorfultrash

import android.graphics.Paint
import android.graphics.RectF

class Drop(x: Float, y: Float, dropSize: Float, canvasWidth: Int, canvasHeight: Int) {

    private var X = x
    private var Y = y
    private var size = dropSize
    private var width = canvasWidth
    private var height = canvasHeight
    private var stepY = 50f
    private var rect = RectF()
    private var paint = Paint()

    var Rect: RectF
        get() = rect
        set(value) {}
    var Paint: Paint
        get() = paint
        set(value) {}

    fun stepForward(){
        Y += stepY
        rect = RectF( X, Y,X+size, Y+size)
    }

    init{
        paint.setARGB(255, (0..255).random(), (0..255).random(), (0..255).random())
        // validate position
        if(X + size >= this.width) {
            X = this.width - size
        }
        if(Y + size >= this.height) {
            Y = this.height - size
        }
        X = if(X<0f) 0f else X
        Y = if(Y<0f) 0f else Y
        // validate canvas size
        width = if(width <=0) (0..1000).random() else width
        height = if(height <=0) (0..1000).random() else height
    }
}