package com.primeiro.homeworkcolorfultrash

import android.graphics.Paint
import android.graphics.RectF
import kotlin.math.cos
import kotlin.math.sin

class Ball(x: Float = 0f, y: Float = 0f, ballSize: Float? = null, canvasWidth: Int = 0, canvasHeight: Int = 0, directionInDegrees: Float? = null, velocity: Float? = null) {
    private var X = x
    private var Y = y
    private var width = canvasWidth
    private var height = canvasHeight
    private var size = 0f
    private var direction = 0f

    private var stepX = 0f
    private var stepY = 0f

    private var rect = RectF()
    private var paint = Paint()

    var Rect: RectF
        get() = rect
        set(value) {}
    var Paint: Paint
        get() = paint
        set(value) {}


    fun stepForward(){

        X += stepX * sin(direction * (Math.PI/180).toFloat())
        if(X + size >= width || X <= 0){
            stepX = -stepX
        }
        Y += stepY * cos(direction * (Math.PI/180).toFloat())
        if(Y + size >= height || Y <= 0){
            stepY = -stepY
        }
        rect = RectF( X, Y,X+size, Y+size)
    }

    init{
        // init & set random values if came null
        rect = RectF( X, Y,X+size, Y+size)
        paint.setARGB(255, (0..255).random(), (0..255).random(), (0..255).random())

        // validate ballSize
        if(ballSize  == null || ballSize > 400 || ballSize <=0) {
            size = (50..400).random().toFloat()
        } else {
            size = ballSize
        }
        // validate direction
        if(directionInDegrees == null || directionInDegrees > 360 || directionInDegrees < -360){
            direction = (0..360).random().toFloat()
        } else {
            direction = directionInDegrees
        }
        // validate velocity
        if (velocity == null || velocity > 100 || velocity <=0) {
            stepX = (5..100).random().toFloat()
            stepY = stepX
        } else {
            stepX = velocity
            stepY = stepX
        }
        // validate position
        if(X + size >= this.width) {
            X = this.width - size
        }
        if(Y + size >= this.height) {
            Y = this.height - size
        }
        X = if(X<0f) 0f else X
        Y = if(Y<0f) 0f else Y
        // validate canvas size
        width = if(width <=0) (0..1000).random() else width
        height = if(height <=0) (0..1000).random() else height
    }
}