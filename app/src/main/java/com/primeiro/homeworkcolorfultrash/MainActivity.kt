package com.primeiro.homeworkcolorfultrash

import android.graphics.Paint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var lastTouchDownXY = FloatArray(2)
    private var buttonIsClicked = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button_run.setOnClickListener {
            button_run.text = "Reset this madness"
            if(buttonIsClicked == true){
                trashcanvasview.raindrops.clear()
                trashcanvasview.circles.clear()
                //trashcanvasview.initCircles(it.width,it.height)
                trashcanvasview.ticks = 0
            }
            val t = Thread{
                animationLoop()
            }
            t.start()
            buttonIsClicked = true
        }

        // get click position
        trashcanvasview.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                when (event?.action) {
                    MotionEvent.ACTION_DOWN -> {
                        lastTouchDownXY[0] = event.getX()
                        lastTouchDownXY[1] = event.getY()
                        // and add ball in position of click
                        var customBall = Ball( lastTouchDownXY[0], lastTouchDownXY[1],
                            null, v!!.width, v.height, null, null)
                        trashcanvasview.circles.add(customBall)
                    }
                }

                return v?.onTouchEvent(event) ?: true
            }
        })
    }

    fun animationLoop(){
        while(true) {
            trashcanvasview.move()
            Thread.sleep(50) // 20fps
        }
    }
}
