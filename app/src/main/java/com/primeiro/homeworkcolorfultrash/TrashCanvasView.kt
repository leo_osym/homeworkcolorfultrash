package com.primeiro.homeworkcolorfultrash

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.View


class TrashCanvasView(context: Context, attributes: AttributeSet) : View(context, attributes)
{
    var circles = ArrayList<Ball>()
    var raindrops = ArrayList<Drop>()
    var ticks = 0

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        // if I set null to some fields -> these fields values will be generated randomly
        initCircles(w,h)
    }

    fun initCircles(canvasWidth: Int, canvasHeight: Int){
        circles.add(Ball(0f,0f,null, canvasWidth, canvasHeight,45f,null))
        circles.add(Ball(0f, canvasHeight.toFloat(), null, canvasWidth, canvasHeight,135f,null))
        circles.add(Ball(canvasWidth.toFloat(),0f, null, canvasWidth, canvasHeight,-45f,null))
        circles.add(Ball(canvasWidth.toFloat(), canvasHeight.toFloat(), null, canvasWidth, canvasHeight,null,null))
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if(canvas == null) return

        for(ball in circles){
            canvas.drawOval(ball.Rect, ball.Paint)
        }
        for(drop in raindrops){
            canvas.drawOval(drop.Rect, drop.Paint)
        }
    }

    fun move(){
        ticks++
        if(ticks % 20 == 0) {
            var drop = Drop((0..width).random().toFloat(),0f,60f, width, height)
            raindrops.add(drop)
        } else if(ticks == 200){
            // clear useless raindrops
            ticks = 0
            raindrops.clear()
        }
        // move circles
        for(ball in circles){
            ball.stepForward()
        }
        // move raindrops
        for(drop in raindrops){
            drop.stepForward()
        }
        postInvalidate()
    }

}